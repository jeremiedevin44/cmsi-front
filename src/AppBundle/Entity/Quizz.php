<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Quizz
 *
 * @ORM\Table(name="quizz")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuizzRepository")
 */
class Quizz
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @var string 
     * 
     * @ORM\Column(name="nom",type="string",length=255) 
     */
    private $nom;
    
    /**
     *
     * @var text
     * 
     * @ORM\Column(name="description",type="text")
     */
    private $description;
    
    /**
    * @ORM\OneToMany(targetEntity="Question_Quizz", mappedBy="quizz")
    */
    private $questions; // 
    
    /**
     * @ORM\OneToMany(targetEntity="Recompense", mappedBy="quizz")
     */
    private $recompenses;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Quizz
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Quizz
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->questions_quizzz = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add questionsQuizzz
     *
     * @param \AppBundle\Entity\Question_Quizz $questionsQuizzz
     *
     * @return Quizz
     */
    public function addQuestionsQuizzz(\AppBundle\Entity\Question_Quizz $questionsQuizzz)
    {
        $this->questions_quizzz[] = $questionsQuizzz;

        return $this;
    }

    /**
     * Remove questionsQuizzz
     *
     * @param \AppBundle\Entity\Question_Quizz $questionsQuizzz
     */
    public function removeQuestionsQuizzz(\AppBundle\Entity\Question_Quizz $questionsQuizzz)
    {
        $this->questions_quizzz->removeElement($questionsQuizzz);
    }

    /**
     * Get questionsQuizzz
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestionsQuizzz()
    {
        return $this->questions_quizzz;
    }

    /**
     * Add questionsQuizz
     *
     * @param \AppBundle\Entity\Question_Quizz $questionsQuizz
     *
     * @return Quizz
     */
    public function addQuestionsQuizz(\AppBundle\Entity\Question_Quizz $questionsQuizz)
    {
        $this->questions_quizz[] = $questionsQuizz;

        return $this;
    }

    /**
     * Remove questionsQuizz
     *
     * @param \AppBundle\Entity\Question_Quizz $questionsQuizz
     */
    public function removeQuestionsQuizz(\AppBundle\Entity\Question_Quizz $questionsQuizz)
    {
        $this->questions_quizz->removeElement($questionsQuizz);
    }

    /**
     * Get questionsQuizz
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestionsQuizz()
    {
        return $this->questions_quizz;
    }

    /**
     * Add question
     *
     * @param \AppBundle\Entity\Question_Quizz $question
     *
     * @return Quizz
     */
    public function addQuestion(\AppBundle\Entity\Question_Quizz $question)
    {
        $this->questions[] = $question;

        return $this;
    }

    /**
     * Remove question
     *
     * @param \AppBundle\Entity\Question_Quizz $question
     */
    public function removeQuestion(\AppBundle\Entity\Question_Quizz $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Add recompense
     *
     * @param \AppBundle\Entity\Recompense $recompense
     *
     * @return Quizz
     */
    public function addRecompense(\AppBundle\Entity\Recompense $recompense)
    {
        $this->recompenses[] = $recompense;

        return $this;
    }

    /**
     * Remove recompense
     *
     * @param \AppBundle\Entity\Recompense $recompense
     */
    public function removeRecompense(\AppBundle\Entity\Recompense $recompense)
    {
        $this->recompenses->removeElement($recompense);
    }

    /**
     * Get recompenses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRecompenses()
    {
        return $this->recompenses;
    }
}
