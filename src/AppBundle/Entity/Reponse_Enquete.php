<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reponse_Enquete
 *
 * @ORM\Table(name="reponse__enquete")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Reponse_EnqueteRepository")
 */
class Reponse_Enquete
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(name="contenu", type="string",length=255)
     */
    public $contenu;
    
    
    /**
    * @ORM\ManyToOne(targetEntity="Question_Enquete", inversedBy="reponses")
    * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
    */
    private $question; //
    
    /**
    * @ORM\OneToMany(targetEntity="utilisateur_reponse_enquete", mappedBy="reponse")
    */
    private $reponses_utilisateur;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     *
     * @return Reponse_Enquete
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set ordre
     *
     * @param integer $ordre
     *
     * @return Reponse_Enquete
     */
    public function setOrdre($ordre)
    {
        $this->ordre = $ordre;

        return $this;
    }

    /**
     * Get ordre
     *
     * @return integer
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * Set reponsesQuestionSequenceEnquete
     *
     * @param \AppBundle\Entity\Question_Enquete $reponsesQuestionSequenceEnquete
     *
     * @return Reponse_Enquete
     */
    public function setReponsesQuestionSequenceEnquete(\AppBundle\Entity\Question_Enquete $reponsesQuestionSequenceEnquete = null)
    {
        $this->reponses_question_sequence_enquete = $reponsesQuestionSequenceEnquete;

        return $this;
    }

    /**
     * Get reponsesQuestionSequenceEnquete
     *
     * @return \AppBundle\Entity\Question_Enquete
     */
    public function getReponsesQuestionSequenceEnquete()
    {
        return $this->reponses_question_sequence_enquete;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reponses_utilisateur = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set question
     *
     * @param \AppBundle\Entity\Question_Enquete $question
     *
     * @return Reponse_Enquete
     */
    public function setQuestion(\AppBundle\Entity\Question_Enquete $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \AppBundle\Entity\Question_Enquete
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Add reponsesUtilisateur
     *
     * @param \AppBundle\Entity\utilisateur_reponse_enquete $reponsesUtilisateur
     *
     * @return Reponse_Enquete
     */
    public function addReponsesUtilisateur(\AppBundle\Entity\utilisateur_reponse_enquete $reponsesUtilisateur)
    {
        $this->reponses_utilisateur[] = $reponsesUtilisateur;

        return $this;
    }

    /**
     * Remove reponsesUtilisateur
     *
     * @param \AppBundle\Entity\utilisateur_reponse_enquete $reponsesUtilisateur
     */
    public function removeReponsesUtilisateur(\AppBundle\Entity\utilisateur_reponse_enquete $reponsesUtilisateur)
    {
        $this->reponses_utilisateur->removeElement($reponsesUtilisateur);
    }

    /**
     * Get reponsesUtilisateur
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReponsesUtilisateur()
    {
        return $this->reponses_utilisateur;
    }
}
