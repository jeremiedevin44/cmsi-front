<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reponse_Quizz
 *
 * @ORM\Table(name="reponse__quizz")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Reponse_QuizzRepository")
 */
class Reponse_Quizz
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @var string 
     * 
     * @ORM\Column(name="contenu",type="string",length=255) 
     */
    private $contenu;
    
    /**
    * @ORM\ManyToOne(targetEntity="Question_Quizz", inversedBy="reponses_quizzz")
    * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
    */
    private $question; // 
    
    /**
    * @ORM\OneToMany(targetEntity="utilisateur_reponse_quizz", mappedBy="reponse")
    */
    private $reponses_utilisateur;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     *
     * @return Reponse_Quizz
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set reponsesQuizz
     *
     * @param \AppBundle\Entity\Question_Quizz $reponsesQuizz
     *
     * @return Reponse_Quizz
     */
    public function setReponsesQuizz(\AppBundle\Entity\Question_Quizz $reponsesQuizz = null)
    {
        $this->reponses_quizz = $reponsesQuizz;

        return $this;
    }

    /**
     * Get reponsesQuizz
     *
     * @return \AppBundle\Entity\Question_Quizz
     */
    public function getReponsesQuizz()
    {
        return $this->reponses_quizz;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reponses_utilisateur = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set question
     *
     * @param \AppBundle\Entity\Question_Quizz $question
     *
     * @return Reponse_Quizz
     */
    public function setQuestion(\AppBundle\Entity\Question_Quizz $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \AppBundle\Entity\Question_Quizz
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Add reponsesUtilisateur
     *
     * @param \AppBundle\Entity\utilisateur_reponse_quizz $reponsesUtilisateur
     *
     * @return Reponse_Quizz
     */
    public function addReponsesUtilisateur(\AppBundle\Entity\utilisateur_reponse_quizz $reponsesUtilisateur)
    {
        $this->reponses_utilisateur[] = $reponsesUtilisateur;

        return $this;
    }

    /**
     * Remove reponsesUtilisateur
     *
     * @param \AppBundle\Entity\utilisateur_reponse_quizz $reponsesUtilisateur
     */
    public function removeReponsesUtilisateur(\AppBundle\Entity\utilisateur_reponse_quizz $reponsesUtilisateur)
    {
        $this->reponses_utilisateur->removeElement($reponsesUtilisateur);
    }

    /**
     * Get reponsesUtilisateur
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReponsesUtilisateur()
    {
        return $this->reponses_utilisateur;
    }
}
