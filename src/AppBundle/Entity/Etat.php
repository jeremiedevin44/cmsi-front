<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Etat
 *
 * @ORM\Table(name="etat")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EtatRepository")
 */
class Etat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Libelle", type="string", length=255)
     */
    private $libelle;
    
    /**
     * @ORM\OneToMany(targetEntity="Sequence", mappedBy="etat")
     */
    private $sequences;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Etat
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sequences = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add sequence
     *
     * @param \AppBundle\Entity\Sequence $sequence
     *
     * @return Etat
     */
    public function addSequence(\AppBundle\Entity\Sequence $sequence)
    {
        $this->sequences[] = $sequence;

        return $this;
    }

    /**
     * Remove sequence
     *
     * @param \AppBundle\Entity\Sequence $sequence
     */
    public function removeSequence(\AppBundle\Entity\Sequence $sequence)
    {
        $this->sequences->removeElement($sequence);
    }

    /**
     * Get sequences
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSequences()
    {
        return $this->sequences;
    }
}
