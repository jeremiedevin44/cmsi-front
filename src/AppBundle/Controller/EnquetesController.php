<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class EnquetesController extends Controller
{
    /**
     * @Route("/Enquetes/", name="listerEnquetes")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        $em=$this->getDoctrine()->getManager();
        $enquetes=$em->getRepository("AppBundle:Enquete")->findAll();
        return $this->render('Enquetes/index.html.twig',["enquetes"=>$enquetes]);
    }
    
    
    /**
     * @Route("/Enquetes/{id}", name="repondreEnquete"
     * , requirements={
     * "id":"\d+"
     * })
     */
    public function repondreEnquete($id,Request $request){       
        
        $em=$this->getDoctrine()->getManager();
        $enquete=$em->getRepository("AppBundle:Enquete")->find($id);
        $sequences=$em->getRepository("AppBundle:Sequence")->findBy(["enquete"=>$id]);
        
        return $this->render('Enquetes/enquete.html.twig',["enquete"=>$enquete,"sequences"=>$sequences]);
    
    }
    
    
    /**
     * @Route("/Enquetes/Sequences/{id}", name="voirQuestion"
     * , requirements={
     * "id":"\d+"
     * })
     */
    public function voirQuestion($id,Request $request){       
        
        $em=$this->getDoctrine()->getManager();
        
        $sequence=$em->getRepository("AppBundle:Sequence")->find($id);
        $nom_enquete=ucfirst($sequence->getEnquete()->getTitre());
        $questions=$em->getRepository("AppBundle:Question_Enquete")->findBy(["sequence"=>$id]);
        $reponses=$em->getRepository("AppBundle:Reponse_Enquete")->findBy(["question"=>$questions]);
        
        
        return $this->render('Enquetes/sequence.html.twig',["nom_enquete"=>$nom_enquete,"sequence"=>$sequence,"questions"=>$questions,"reponses"=>$reponses]);
    
    }
    
    
    /**
     * @Route("/Enquetes/Sequences/Reponses/enregistrerReponse", name="enregistrerReponse")
     */
    public function enregistrerReponse(Request $request)
    {   
        $statut=$this->isConnected();
        if ($statut==false) {
            $this->addFlash('danger', "Vous devez être connecté pour répondre aux enquêtes.");
            return $this->render("Utilisateurs/connexion.html.twig");
        }
        else{
            
            $em=$this->getDoctrine()->getManager();
            
            
            $utilisateur=$em->getRepository("AppBundle:Utilisateur")->findOneById($statut);
            
            if ($request->isMethod('POST')) {
                $reponses=$_POST;
                foreach ($reponses as $reponseX){
                    $reponse_utilisateur = new \AppBundle\Entity\utilisateur_reponse_enquete();
                    $reponse = $em->getRepository("AppBundle:Reponse_Enquete")->findOneById($reponseX);
                    $reponse_utilisateur->setUtilisateur($utilisateur);
                    $reponse_utilisateur->setReponse($reponse);
                    $dateactuelle = new \DateTime(date("Y-m-d h:i:s"));
                    $reponse_utilisateur->setDateReponse($dateactuelle);

                    $em->persist($reponse_utilisateur);
                    $em->flush();
                }

                $this->addFlash('notice', "Vos réponses ont bien été enregistrées !");
            }
            $enquetes=$em->getRepository("AppBundle:Enquete")->findAll();
            return $this->render('Enquetes/index.html.twig',["enquetes"=>$enquetes]);
        }
    }
    
        
    
  
    
}

    
