<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ReponsesController extends Controller
{
    /**
     * @Route("/ReponsesSondage/voir/{id}", name="affichageReponsesSondage"
     * , requirements={
     * "id":"\d+"
     * })
     */
    public function affichageReponsesSondage($id,Request $request)
    {
        $statut=$this->isConnected();
        if ($statut==false) {
            return $this->render('Utilisateurs/index.html.twig');
        }
        else{
            $em=$this->getDoctrine()->getManager();
            $sondage=$em->getRepository("AppBundle:Sondage")->find($id);
            $questions=$em->getRepository("AppBundle:Question_Sondage")->findBy(["sondage"=>$id]);
            $reponses=$em->getRepository("AppBundle:Reponse_Sondage")->findBy(["question"=>$id]);
            $reponses_utilisateur=$em->getRepository("AppBundle:utilisateur_reponse_sondage")->findBy(["utilisateur"=>$statut]);
            
            $tab_reponses_sondage=$em->getRepository("AppBundle:Question_Sondage")->createQueryBuilder('trs')
                    ->Join("trs.reponses","r")
                    ->leftJoin("r.reponses_utilisateur","urs")
                    ->groupBy("r.id")
                    ->select("r.id,Count(urs.reponse),r.contenu")
                    ->where("trs.id = :id")
                    ->setParameter("id",$id)
                    ->getQuery()
                    ->getResult();
           
            $donnees_graph=json_encode($tab_reponses_sondage);
            return $this->render('Reponses/index.html.twig',["element"=>"sondage","sondage"=>$sondage,"questions"=>$questions,"reponses"=>$reponses,"reponses_utilisateur"=>$reponses_utilisateur,"tab"=>$tab_reponses_sondage,"donnees_graph"=>$donnees_graph]);
        }
    }
    
    
    /**
     * @Route("/ReponsesQuizz/voir/{id}", name="affichageReponsesQuizz"
     * , requirements={
     * "id":"\d+"
     * })
     */
    public function affichageReponsesQuizz($id,Request $request)
    {
        $statut=$this->isConnected();
        if ($statut==false) {
            return $this->render('Utilisateurs/index.html.twig');
        }
        else{
            $em=$this->getDoctrine()->getManager();
            $quizz=$em->getRepository("AppBundle:Quizz")->find($id);
            $questions=$em->getRepository("AppBundle:Question_Quizz")->findBy(["quizz"=>$id]);
            $reponses=$em->getRepository("AppBundle:Reponse_Quizz")->findBy(["question"=>$questions]);
            $reponses_utilisateur=$em->getRepository("AppBundle:utilisateur_reponse_quizz")->findBy(["utilisateur"=>$statut]);
            $recompenses=$em->getRepository("AppBundle:Recompense")->findBy(["quizz"=>$quizz]);
            
            
            
            
            
            $nbrrep = $em->getRepository("AppBundle:Quizz")->createQueryBuilder('trs')
                    ->Join("trs.questions","q")
                    ->Join("q.reponses_quizzz","r")
                    ->leftJoin("r.reponses_utilisateur","urs")
                    ->groupBy("r.id")
                    ->select("r.id,Count(urs.reponse) AS nbr ,q.id,q.libelle,r.contenu AS reponse")
                    ->where("trs.id = :id")
                    ->setParameter("id",$id)
                    ->getQuery()
                    ->getResult();

            $i = 0;
            $id = 0;
            foreach ($nbrrep as $rep) {
                if ($id != $rep["id"]) {
                    $id = $rep["id"];
                    $i = 0;
                    $questionsd[$id][0] = $rep["id"];
                }
                $questionsd[$id][1][$i] = $rep["reponse"];
                $questionsd[$id][2][$i] = $rep["nbr"];
                $i++;
            }
            
            
            $donnees_graph=json_encode($nbrrep);
            $donnees_graph_sup= json_encode($nbrrep);
            
            return $this->render('Reponses/index.html.twig',["element"=>"quizz","quizz"=>$quizz,"questions"=>$questions,"reponses"=>$reponses,"reponses_utilisateur"=>$reponses_utilisateur,"tab"=>$nbrrep,"donnees_graph"=>$donnees_graph,"nbr" => $questionsd,"donnees_graph_sup"=>$donnees_graph_sup,"recompenses"=>$recompenses]);
        }
    }  
    
}