<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sequence
 *
 * @ORM\Table(name="sequence")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SequenceRepository")
 */
class Sequence
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreation", type="date", nullable=true)
     */
    private $dateCreation;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFin", type="date", nullable=true)
     */
    private $dateFin;
    
        
    /**
     *
     * @var string 
     * 
     * @ORM\Column(name="nom",type="string",length=255) 
     */
    private $nom;
    
    /**
    * @ORM\ManyToOne(targetEntity="Enquete", inversedBy="sequences")
    * @ORM\JoinColumn(name="enquete_id", referencedColumnName="id")
    */
    private $enquete; // 
    
    /**
     * @ORM\OneToMany(targetEntity="Question_Enquete", mappedBy="sequence")
     */
    private $questions;
    
    /**
    * @ORM\ManyToOne(targetEntity="Etat", inversedBy="sequences")
    * @ORM\JoinColumn(name="etat_id", referencedColumnName="id")
    */
    private $etat;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return Sequence
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     *
     * @return Sequence
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set etat
     *
     * @param boolean $etat
     *
     * @return Sequence
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return boolean
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Sequence
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set sequencesEnquete
     *
     * @param \AppBundle\Entity\Enquete $sequencesEnquete
     *
     * @return Sequence
     */
    public function setSequencesEnquete(\AppBundle\Entity\Enquete $sequencesEnquete = null)
    {
        $this->sequences_enquete = $sequencesEnquete;

        return $this;
    }

    /**
     * Get sequencesEnquete
     *
     * @return \AppBundle\Entity\Enquete
     */
    public function getSequencesEnquete()
    {
        return $this->sequences_enquete;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->questions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set enquete
     *
     * @param \AppBundle\Entity\Enquete $enquete
     *
     * @return Sequence
     */
    public function setEnquete(\AppBundle\Entity\Enquete $enquete = null)
    {
        $this->enquete = $enquete;

        return $this;
    }

    /**
     * Get enquete
     *
     * @return \AppBundle\Entity\Enquete
     */
    public function getEnquete()
    {
        return $this->enquete;
    }

    /**
     * Add question
     *
     * @param \AppBundle\Entity\Question_Enquete $question
     *
     * @return Sequence
     */
    public function addQuestion(\AppBundle\Entity\Question_Enquete $question)
    {
        $this->questions[] = $question;

        return $this;
    }

    /**
     * Remove question
     *
     * @param \AppBundle\Entity\Question_Enquete $question
     */
    public function removeQuestion(\AppBundle\Entity\Question_Enquete $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }
}
