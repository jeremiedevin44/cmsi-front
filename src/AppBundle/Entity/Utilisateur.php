<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Utilisateur
 *
 * @ORM\Table(name="utilisateur")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UtilisateurRepository")
 */
class Utilisateur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @var string 
     * 
     * @ORM\Column(name="nom",type="string",length=255, nullable=true) 
     */
    private $nom;
    
    /**
     *
     * @var string 
     * 
     * @ORM\Column(name="prenom",type="string",length=255, nullable=true) 
     */
    private $prenom;
    
    /**
     *
     * @var string 
     * 
     * @ORM\Column(name="mail",type="string",length=255, nullable=true) 
     */
    private $mail;
    
    /**
     *
     * @var string 
     * 
     * @ORM\Column(name="password",type="string",length=255, nullable=true) 
     */
    private $password;
    
    /**
     *
     * @var string 
     * 
     * @ORM\Column(name="ip",type="string",length=15, nullable=true) 
     */
    private $ip;
    
    /**
     *
     * @ORM\ManyToMany(targetEntity="Quizz")
     * @ORM\JoinTable(name="utilisateur_quizz")
     */
    private $quizzs;
    
    
    
    /**
    * @ORM\OneToMany(targetEntity="sondage_utilisateur", mappedBy="utilisateur")
    */
    private $sondages;
    
    /**
     *
     * @ORM\ManyToMany(targetEntity="Enquete")
     * @ORM\JoinTable(name="uilisateur_enquete")
     */
    private $enquetes;
    
        
    /**
    * @ORM\OneToMany(targetEntity="utilisateur_reponse_sondage", mappedBy="utilisateur")
    */
    private $reponses_sondage;
    
    /**
    * @ORM\OneToMany(targetEntity="utilisateur_reponse_enquete", mappedBy="utilisateur")
    */
    private $reponses_enquetes;
    
    /**
    * @ORM\OneToMany(targetEntity="utilisateur_reponse_quizz", mappedBy="utilisateur")
    */
    private $reponses_quizz;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->quizz = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Utilisateur
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Utilisateur
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set mail
     *
     * @param string $mail
     *
     * @return Utilisateur
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Utilisateur
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Add quizz
     *
     * @param \AppBundle\Entity\Quizz $quizz
     *
     * @return Utilisateur
     */
    public function addQuizz(\AppBundle\Entity\Quizz $quizz)
    {
        $this->quizz[] = $quizz;

        return $this;
    }

    /**
     * Remove quizz
     *
     * @param \AppBundle\Entity\Quizz $quizz
     */
    public function removeQuizz(\AppBundle\Entity\Quizz $quizz)
    {
        $this->quizz->removeElement($quizz);
    }

    /**
     * Get quizz
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuizz()
    {
        return $this->quizz;
    }

    /**
     * Add reponsesQuizz
     *
     * @param \AppBundle\Entity\Reponse_Quizz $reponsesQuizz
     *
     * @return Utilisateur
     */
    public function addReponsesQuizz(\AppBundle\Entity\Reponse_Quizz $reponsesQuizz)
    {
        $this->reponses_quizz[] = $reponsesQuizz;

        return $this;
    }

    /**
     * Remove reponsesQuizz
     *
     * @param \AppBundle\Entity\Reponse_Quizz $reponsesQuizz
     */
    public function removeReponsesQuizz(\AppBundle\Entity\Reponse_Quizz $reponsesQuizz)
    {
        $this->reponses_quizz->removeElement($reponsesQuizz);
    }

    /**
     * Get reponsesQuizz
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReponsesQuizz()
    {
        return $this->reponses_quizz;
    }

    /**
     * Add sondage
     *
     * @param \AppBundle\Entity\Sondage $sondage
     *
     * @return Utilisateur
     */
    public function addSondage(\AppBundle\Entity\Sondage $sondage)
    {
        $this->sondages[] = $sondage;

        return $this;
    }

    /**
     * Remove sondage
     *
     * @param \AppBundle\Entity\Sondage $sondage
     */
    public function removeSondage(\AppBundle\Entity\Sondage $sondage)
    {
        $this->sondages->removeElement($sondage);
    }

    /**
     * Get sondages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSondages()
    {
        return $this->sondages;
    }

    /**
     * Add reponsesSondage
     *
     * @param \AppBundle\Entity\Reponse_Sondage $reponsesSondage
     *
     * @return Utilisateur
     */
    public function addReponsesSondage(\AppBundle\Entity\Reponse_Sondage $reponsesSondage)
    {
        $this->reponses_sondages[] = $reponsesSondage;

        return $this;
    }

    /**
     * Remove reponsesSondage
     *
     * @param \AppBundle\Entity\Reponse_Sondage $reponsesSondage
     */
    public function removeReponsesSondage(\AppBundle\Entity\Reponse_Sondage $reponsesSondage)
    {
        $this->reponses_sondages->removeElement($reponsesSondage);
    }

    /**
     * Get reponsesSondages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReponsesSondages()
    {
        return $this->reponses_sondages;
    }

    /**
     * Add enquete
     *
     * @param \AppBundle\Entity\Enquete $enquete
     *
     * @return Utilisateur
     */
    public function addEnquete(\AppBundle\Entity\Enquete $enquete)
    {
        $this->enquetes[] = $enquete;

        return $this;
    }

    /**
     * Remove enquete
     *
     * @param \AppBundle\Entity\Enquete $enquete
     */
    public function removeEnquete(\AppBundle\Entity\Enquete $enquete)
    {
        $this->enquetes->removeElement($enquete);
    }

    /**
     * Get enquetes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnquetes()
    {
        return $this->enquetes;
    }

    /**
     * Add reponsesEnquete
     *
     * @param \AppBundle\Entity\Reponse_Enquete $reponsesEnquete
     *
     * @return Utilisateur
     */
    public function addReponsesEnquete(\AppBundle\Entity\Reponse_Enquete $reponsesEnquete)
    {
        $this->reponses_enquetes[] = $reponsesEnquete;

        return $this;
    }

    /**
     * Remove reponsesEnquete
     *
     * @param \AppBundle\Entity\Reponse_Enquete $reponsesEnquete
     */
    public function removeReponsesEnquete(\AppBundle\Entity\Reponse_Enquete $reponsesEnquete)
    {
        $this->reponses_enquetes->removeElement($reponsesEnquete);
    }

    /**
     * Get reponsesEnquetes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReponsesEnquetes()
    {
        return $this->reponses_enquetes;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return Utilisateur
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Get quizzs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuizzs()
    {
        return $this->quizzs;
    }

    /**
     * Get reponsesSondage
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReponsesSondage()
    {
        return $this->reponses_sondage;
    }
}
