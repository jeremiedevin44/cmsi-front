<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * sondage_utilisateur
 *
 * @ORM\Table(name="sondage_utilisateur")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\sondage_utilisateurRepository")
 */
class sondage_utilisateur
{
    /**
    * @ORM\Id @ORM\ManyToOne(targetEntity="Utilisateur", inversedBy="sondages")
    * @ORM\JoinColumn(name="utilisateur_id", referencedColumnName="id")
    */
    private $utilisateur;
    
    /**
    * @ORM\Id @ORM\ManyToOne(targetEntity="Sondage", inversedBy="utilisateurs")
    * @ORM\JoinColumn(name="reponse_id", referencedColumnName="id")
    */
    private $sondage;

    /**
     * @var bool
     *
     * @ORM\Column(name="SondageFini", type="boolean")
     */
    private $sondageFini;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sondageFini
     *
     * @param boolean $sondageFini
     *
     * @return sondage_utilisateur
     */
    public function setSondageFini($sondageFini)
    {
        $this->sondageFini = $sondageFini;

        return $this;
    }

    /**
     * Get sondageFini
     *
     * @return bool
     */
    public function getSondageFini()
    {
        return $this->sondageFini;
    }

    /**
     * Set utilisateur
     *
     * @param \AppBundle\Entity\Utilisateur $utilisateur
     *
     * @return sondage_utilisateur
     */
    public function setUtilisateur(\AppBundle\Entity\Utilisateur $utilisateur)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \AppBundle\Entity\Utilisateur
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Set sondage
     *
     * @param \AppBundle\Entity\Sondage $sondage
     *
     * @return sondage_utilisateur
     */
    public function setSondage(\AppBundle\Entity\Sondage $sondage)
    {
        $this->sondage = $sondage;

        return $this;
    }

    /**
     * Get sondage
     *
     * @return \AppBundle\Entity\Sondage
     */
    public function getSondage()
    {
        return $this->sondage;
    }
}
