<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sondage
 *
 * @ORM\Table(name="sondages")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SondagesRepository")
 */
class Sondage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @var string 
     * 
     * @ORM\Column(name="titre",type="string",length=255) 
     */
    private $titre;
    
    /**
     *
     * @var text
     * 
     * @ORM\Column(name="description",type="text")
     */
    private $description;
    
    
    /**
   * @ORM\OneToMany(targetEntity="Question_Sondage", mappedBy="sondage")
   */
    private $questions_sondage; // 
    
    /**
     *
     * @ORM\ManyToMany(targetEntity="Theme")
     * @ORM\JoinTable(name="theme_sondage")
     */
    private $themes;
    
    /**
    * @ORM\OneToMany(targetEntity="sondage_utilisateur", mappedBy="sondage")
    */
    private $utilisateurs;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Sondages
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Sondages
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->questions_sondage = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add questionsSondage
     *
     * @param \AppBundle\Entity\Question_Sondage $questionsSondage
     *
     * @return Sondage
     */
    public function addQuestionsSondage(\AppBundle\Entity\Question_Sondage $questionsSondage)
    {
        $this->questions_sondage[] = $questionsSondage;

        return $this;
    }

    /**
     * Remove questionsSondage
     *
     * @param \AppBundle\Entity\Question_Sondage $questionsSondage
     */
    public function removeQuestionsSondage(\AppBundle\Entity\Question_Sondage $questionsSondage)
    {
        $this->questions_sondage->removeElement($questionsSondage);
    }

    /**
     * Get questionsSondage
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestionsSondage()
    {
        return $this->questions_sondage;
    }

    /**
     * Add theme
     *
     * @param \AppBundle\Entity\Theme $theme
     *
     * @return Sondage
     */
    public function addTheme(\AppBundle\Entity\Theme $theme)
    {
        $this->themes[] = $theme;

        return $this;
    }

    /**
     * Remove theme
     *
     * @param \AppBundle\Entity\Theme $theme
     */
    public function removeTheme(\AppBundle\Entity\Theme $theme)
    {
        $this->themes->removeElement($theme);
    }

    /**
     * Get themes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getThemes()
    {
        return $this->themes;
    }

    /**
     * Add utilisateur
     *
     * @param \AppBundle\Entity\sondage_utilisateur $utilisateur
     *
     * @return Sondage
     */
    public function addUtilisateur(\AppBundle\Entity\sondage_utilisateur $utilisateur)
    {
        $this->utilisateurs[] = $utilisateur;

        return $this;
    }

    /**
     * Remove utilisateur
     *
     * @param \AppBundle\Entity\sondage_utilisateur $utilisateur
     */
    public function removeUtilisateur(\AppBundle\Entity\sondage_utilisateur $utilisateur)
    {
        $this->utilisateurs->removeElement($utilisateur);
    }

    /**
     * Get utilisateurs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUtilisateurs()
    {
        return $this->utilisateurs;
    }
}
