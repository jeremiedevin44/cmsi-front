<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reponse_Sondage
 *
 * @ORM\Table(name="reponse__sondage")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Reponse_SondageRepository")
 */
class Reponse_Sondage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    
    /**
     *
     * @var string 
     * 
     * @ORM\Column(name="contenu",type="string",length=255) 
     */
    private $contenu;
    
    /**
    * @ORM\ManyToOne(targetEntity="Question_Sondage", inversedBy="reponses")
    * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
    */
    private $question; //
    
    /**
    * @ORM\OneToMany(targetEntity="utilisateur_reponse_sondage", mappedBy="reponse")
    */
    private $reponses_utilisateur;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     *
     * @return Reponse_Sondage
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set reponsesSondage
     *
     * @param \AppBundle\Entity\Question_Sondage $reponsesSondage
     *
     * @return Reponse_Sondage
     */
    public function setReponsesSondage(\AppBundle\Entity\Question_Sondage $reponsesSondage = null)
    {
        $this->reponses_sondage = $reponsesSondage;

        return $this;
    }

    /**
     * Get reponsesSondage
     *
     * @return \AppBundle\Entity\Question_Sondage
     */
    public function getReponsesSondage()
    {
        return $this->reponses_sondage;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reponses_utilisateur = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add reponsesUtilisateur
     *
     * @param \AppBundle\Entity\utilisateur__reponse__sondage $reponsesUtilisateur
     *
     * @return Reponse_Sondage
     */
    public function addReponsesUtilisateur(\AppBundle\Entity\utilisateur__reponse__sondage $reponsesUtilisateur)
    {
        $this->reponses_utilisateur[] = $reponsesUtilisateur;

        return $this;
    }

    /**
     * Remove reponsesUtilisateur
     *
     * @param \AppBundle\Entity\utilisateur__reponse__sondage $reponsesUtilisateur
     */
    public function removeReponsesUtilisateur(\AppBundle\Entity\utilisateur__reponse__sondage $reponsesUtilisateur)
    {
        $this->reponses_utilisateur->removeElement($reponsesUtilisateur);
    }

    /**
     * Get reponsesUtilisateur
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReponsesUtilisateur()
    {
        return $this->reponses_utilisateur;
    }

    /**
     * Set question
     *
     * @param \AppBundle\Entity\Question_Sondage $question
     *
     * @return Reponse_Sondage
     */
    public function setQuestion(\AppBundle\Entity\Question_Sondage $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \AppBundle\Entity\Question_Sondage
     */
    public function getQuestion()
    {
        return $this->question;
    }
}
