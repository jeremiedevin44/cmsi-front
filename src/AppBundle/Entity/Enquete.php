<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Enquete
 *
 * @ORM\Table(name="enquetes")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EnquetesRepository")
 */
class Enquete
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @var string 
     * 
     * @ORM\Column(name="titre",type="string",length=255) 
     */
    private $titre;
    
    /**
     *
     * @var text
     * 
     * @ORM\Column(name="description",type="text")
     */
    private $description;
    
    /**
     *
     * @ORM\ManyToMany(targetEntity="Theme")
     * @ORM\JoinTable(name="theme_enquete")
     */
    private $themes;
    
    /**
     * @ORM\OneToMany(targetEntity="Sequence", mappedBy="enquete")
     */
    private $sequences;
   


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Enquetes
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Enquetes
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->questions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add theme
     *
     * @param \AppBundle\Entity\Theme $theme
     *
     * @return Enquete
     */
    public function addTheme(\AppBundle\Entity\Theme $theme)
    {
        $this->themes[] = $theme;

        return $this;
    }

    /**
     * Remove theme
     *
     * @param \AppBundle\Entity\Theme $theme
     */
    public function removeTheme(\AppBundle\Entity\Theme $theme)
    {
        $this->themes->removeElement($theme);
    }

    /**
     * Get themes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getThemes()
    {
        return $this->themes;
    }

    /**
     * Add sequence
     *
     * @param \AppBundle\Entity\Sequence $sequence
     *
     * @return Enquete
     */
    public function addSequence(\AppBundle\Entity\Sequence $sequence)
    {
        $this->sequences[] = $sequence;

        return $this;
    }

    /**
     * Remove sequence
     *
     * @param \AppBundle\Entity\Sequence $sequence
     */
    public function removeSequence(\AppBundle\Entity\Sequence $sequence)
    {
        $this->sequences->removeElement($sequence);
    }

    /**
     * Get sequences
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSequences()
    {
        return $this->sequences;
    }
}
