<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em= $this->getDoctrine()->getManager();
        $themes=$em->getRepository("AppBundle:Theme")->findAll();
        return $this->render('default/index.html.twig',["themes"=>$themes]);
        // , [ 'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,]
    }
    
    /**
     * @Route("/Themes/{id}", name="triTheme"
     * , requirements={
     * "id":"\d+"
     * })
     */
    public function triTheme($id,Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $theme=$em->getRepository("AppBundle:Theme")->findOneBy(["id"=>$id]);
        
        $enquetes=$em->getRepository("AppBundle:Enquete")
                ->createQueryBuilder("e")
                ->innerJoin("e.themes","q")
                ->Where("q.id=:theme_id")
                ->setParameter("theme_id",$id)
                ->getQuery()
                ->getResult();
        $sondages=$em->getRepository("AppBundle:Sondage")
                ->createQueryBuilder("e")
                ->innerJoin("e.themes","q")
                ->Where("q.id=:theme_id")
                ->setParameter("theme_id",$id)
                ->getQuery()
                ->getResult();
        
        return $this->render('default/themes.html.twig',["theme"=>$theme, "enquetes"=>$enquetes,"sondages"=>$sondages]);
    }
    
    
    /**
     * @Route("/Sondages/", name="listerSondages")
     */
    public function listerSondages(Request $request)
    {
        // replace this example code with whatever you need
        $em=$this->getDoctrine()->getManager();
        $sondages=$em->getRepository("AppBundle:Sondage")->findAll();
        return $this->render('Sondages/index.html.twig',["sondages"=>$sondages]);
    }
    
    /**
     * @Route("/Quizz/", name="listerQuizz")
     */
    public function listerQuizz(Request $request)
    {
        // replace this example code with whatever you need
        $em=$this->getDoctrine()->getManager();
        $quizz=$em->getRepository("AppBundle:Quizz")->findAll();
        return $this->render('Quizz/index.html.twig',["quizz"=>$quizz]);
    }
    
    /**
     * @Route("/rechercheContenu", name="rechercheContenu")
     */
    public function rechercherContenu(Request $request)
    {
        $recherche=$_POST['recherche'];
        $em=$this->getDoctrine()->getManager();
        $contenu_quizz=$em->getRepository("AppBundle:Quizz")->findBy(["nom"=>$recherche]);
        $contenu_sondage=$em->getRepository("AppBundle:Sondage")->findBy(["titre"=>$recherche]);
        $contenu_enquete=$em->getRepository("AppBundle:Enquete")->findBy(["titre"=>$recherche]);
        $contenu_theme=$em->getRepository("AppBundle:Theme")->findBy(["nom"=>$recherche]);
        
        return $this->render("default/recherche.html.twig",["recherche"=>$recherche,"contenu_quizz"=>$contenu_quizz,"contenu_sondage"=>$contenu_sondage,"contenu_enquete"=>$contenu_enquete,"contenu_theme"=>$contenu_theme]);
    }
       
}
