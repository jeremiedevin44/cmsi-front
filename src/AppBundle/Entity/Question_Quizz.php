<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Question_Quizz
 *
 * @ORM\Table(name="question__quizz")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Question_QuizzRepository")
 */
class Question_Quizz
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @var string 
     * 
     * @ORM\Column(name="libelle",type="string",length=255) 
     */
    private $libelle;

    
    /**
     * @ORM\ManyToOne(targetEntity="Quizz", inversedBy="questions")
     * @ORM\JoinColumn(name="quizz_id", referencedColumnName="id")
     */
    private $quizz; 
    
    
    /**
     * @ORM\OneToMany(targetEntity="Reponse_Quizz", mappedBy="question")
     */
    private $reponses_quizzz;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reponses_quizzz = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Question_Quizz
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Add reponsesQuizzz
     *
     * @param \AppBundle\Entity\Reponse_Quizz $reponsesQuizzz
     *
     * @return Question_Quizz
     */
    public function addReponsesQuizzz(\AppBundle\Entity\Reponse_Quizz $reponsesQuizzz)
    {
        $this->reponses_quizzz[] = $reponsesQuizzz;

        return $this;
    }

    /**
     * Remove reponsesQuizzz
     *
     * @param \AppBundle\Entity\Reponse_Quizz $reponsesQuizzz
     */
    public function removeReponsesQuizzz(\AppBundle\Entity\Reponse_Quizz $reponsesQuizzz)
    {
        $this->reponses_quizzz->removeElement($reponsesQuizzz);
    }

    /**
     * Get reponsesQuizzz
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReponsesQuizzz()
    {
        return $this->reponses_quizzz;
    }

    /**
     * Set questionsQuizz
     *
     * @param \AppBundle\Entity\Quizz $questionsQuizz
     *
     * @return Question_Quizz
     */
    public function setQuestionsQuizz(\AppBundle\Entity\Quizz $questionsQuizz = null)
    {
        $this->questions_quizz = $questionsQuizz;

        return $this;
    }

    /**
     * Get questionsQuizz
     *
     * @return \AppBundle\Entity\Quizz
     */
    public function getQuestionsQuizz()
    {
        return $this->questions_quizz;
    }

    /**
     * Set quizz
     *
     * @param \AppBundle\Entity\Quizz $quizz
     *
     * @return Question_Quizz
     */
    public function setQuizz(\AppBundle\Entity\Quizz $quizz = null)
    {
        $this->quizz = $quizz;

        return $this;
    }

    /**
     * Get quizz
     *
     * @return \AppBundle\Entity\Quizz
     */
    public function getQuizz()
    {
        return $this->quizz;
    }
}
