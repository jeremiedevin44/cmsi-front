<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SondagesController extends Controller
{
    /**
     * @Route("/Sondages/", name="listerSondages")
     */
    public function indexAction(Request $request)
    {
        // fonction qui récupère tous les sondages pour les afficher dans un tableau
        $em=$this->getDoctrine()->getManager();
        $sondages=$em->getRepository("AppBundle:Sondage")->findAll();
        return $this->render('Sondages/index.html.twig',["sondages"=>$sondages]);
    }
    
    
    /**
     * @Route("/Sondages/voir/{id}", name="voirSondage"
     * , requirements={
     * "id":"\d+"
     * })
     */
    public function voirSondage($id,Request $request)
    {
        // fonction qui récupère grâce à l'id en paramètre (url) le sondage, ses questions et les réponses possibles
        $em=$this->getDoctrine()->getManager();
        $sondage=$em->getRepository("AppBundle:Sondage")->find($id);
        $questions=$em->getRepository("AppBundle:Question_Sondage")->findBy(["sondage"=>$id]);
        $reponses=$em->getRepository("AppBundle:Reponse_Sondage")->findBy(["question"=>$id]);
        return $this->render('Sondages/voir.html.twig',["sondage"=>$sondage,"questions"=>$questions,"reponses"=>$reponses]);
    }
    
   
    
    /**
     * @Route("/Sondages/repondre/enregistrerReponseSondage/{id}", name="enregistrerReponseSondage"
     * , requirements={
     * "id":"\d+"
     * })
     */
    public function enregistrerReponseSondage($id,Request $request)
    {   
        // fonction qui gère l'enregistrement des réponses utilisateur au sondage et qui vérifie avant ça qu'il n'y pas déjà eu de réponses
        $statut=$this->isConnected();
        if ($statut==false) {
            // si l'utilisateur n'est pas connecté on lui affiche une erreur
            $this->addFlash('danger', "Vous devez être connecté pour répondre aux enquêtes.");
            return $this->render("Utilisateurs/connexion.html.twig");
        }
        else{
            // on vérifie qu'il n'y a pas déjà eu de réponse au sondage par l'utilisateur
            $em=$this->getDoctrine()->getManager();
            $sondage=$em->getRepository("AppBundle:Sondage")->find($id);
            $id_sondage=$sondage->getId();
            
            $utilisateur=$em->getRepository("AppBundle:Utilisateur")->findOneById($statut);
            $id_utilisateur=$utilisateur->getId();
            
             $verif_utilisateur_reponses_sondage=$em->getRepository("AppBundle:utilisateur")->createQueryBuilder('u')
                    ->Join("u.reponses_sondage","urs")
                    ->Join("urs.reponse","rs")
                    ->Join("rs.question","qs")
                    ->Join("qs.sondage","q")
                    ->select("q.id")
                    ->where("u.id = :id_utilisateur")
                    ->andWhere("q.id = :id_sondage")
                    ->setParameter("id_utilisateur",$id_utilisateur)
                    ->setParameter("id_sondage",$id_sondage)
                    ->getQuery()
                    ->getResult();
            
            if ($request->isMethod('POST')) {
                $reponses=$_POST;
                foreach ($reponses as $reponseX){
                    $reponse_utilisateur = new \AppBundle\Entity\utilisateur_reponse_sondage();
                    $reponse = $em->getRepository("AppBundle:Reponse_Sondage")->findOneById($reponseX);
                    if (empty($verif_utilisateur_reponses_sondage)) {
                        $reponse_utilisateur->setUtilisateur($utilisateur);
                        $reponse_utilisateur->setReponse($reponse);
                        $dateactuelle = new \DateTime(date("Y-m-d h:i:s"));
                        $reponse_utilisateur->setDateReponse($dateactuelle);
                        
                        // on insère la réponse
                        $em->persist($reponse_utilisateur);
                        $em->flush();
                        $this->addFlash('notice', "Votre réponse a bien été enregistrée !");
                    }
                    else{
                        $this->addFlash('error', "Vous avez déjà répondu à ce sondage !");
                    }
                }
            }
            $sondages=$em->getRepository("AppBundle:Sondage")->findAll();
            // fin du traitement , retour à la liste des sondages
            return $this->render('Sondages/index.html.twig',["sondages"=>$sondages]);
        }
    }
  
    
}

    
