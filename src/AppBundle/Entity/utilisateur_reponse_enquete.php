<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * utilisateur_reponse_enquete
 *
 * @ORM\Table(name="utilisateur_reponse_enquete")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\utilisateur_reponse_enqueteRepository")
 */
class utilisateur_reponse_enquete
{
    /**
    * @ORM\Id @ORM\ManyToOne(targetEntity="Utilisateur", inversedBy="reponses_enquetes")
    * @ORM\JoinColumn(name="utilisateur_id", referencedColumnName="id")
    */
    private $utilisateur;
    
    /**
    * @ORM\Id @ORM\ManyToOne(targetEntity="Reponse_Enquete", inversedBy="reponses_utilisateur")
    * @ORM\JoinColumn(name="reponse_id", referencedColumnName="id")
    */
    private $reponse;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateReponse", type="datetime")
     */
    private $dateReponse;


    /**
     * Set dateReponse
     *
     * @param \DateTime $dateReponse
     *
     * @return utilisateur_reponse_enquete
     */
    public function setDateReponse($dateReponse)
    {
        $this->dateReponse = $dateReponse;

        return $this;
    }

    /**
     * Get dateReponse
     *
     * @return \DateTime
     */
    public function getDateReponse()
    {
        return $this->dateReponse;
    }

    /**
     * Set utilisateur
     *
     * @param \AppBundle\Entity\Utilisateur $utilisateur
     *
     * @return utilisateur_reponse_enquete
     */
    public function setUtilisateur(\AppBundle\Entity\Utilisateur $utilisateur)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \AppBundle\Entity\Utilisateur
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Set reponse
     *
     * @param \AppBundle\Entity\Reponse_Enquete $reponse
     *
     * @return utilisateur_reponse_enquete
     */
    public function setReponse(\AppBundle\Entity\Reponse_Enquete $reponse)
    {
        $this->reponse = $reponse;

        return $this;
    }

    /**
     * Get reponse
     *
     * @return \AppBundle\Entity\Reponse_Enquete
     */
    public function getReponse()
    {
        return $this->reponse;
    }
}
