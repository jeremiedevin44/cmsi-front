<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UtilisateursController extends Controller
{
    /**
     * @Route("/Utilisateurs/", name="gestionUtilisateurs")
     */
    public function indexAction(Request $request)
    {
        $statut=$this->isConnected();
        if ($statut==false) {
            return $this->render('Utilisateurs/index.html.twig');
        }
        else{
            return $this->redirectToRoute("infosUtilisateurs");
        }
    }
       
    
    /**
     * @Route("/Utilisateurs/inscriptionUtilisateurs", name="inscriptionUtilisateurs")
     */
    public function inscriptionUtilisateurs(Request $request)
    {
        $statut=$this->isConnected();
        if ($statut==false) {
            // je crée un objet vide
        $utilisateur=new \AppBundle\Entity\Utilisateur();
        
        // je crée au formulaire pour cet objet
        $form=$this->createForm(\AppBundle\Form\UtilisateurType::class, $utilisateur);
        
        // traitement du retour
        if ($form->handleRequest($request)->isValid()) {
            $em=$this->getDoctrine()->getManager();
            $em->persist($utilisateur);
            $em->flush();
            
            $this->addFlash('notice', "".$utilisateur->getNom(). " " . $utilisateur->getPrenom()." êtes le bienvenue chez nous !");
            return $this->redirectToRoute("inscriptionUtilisateurs");
        }
        
        // ici je gérerai le retour en POST...
        return $this->render("Utilisateurs/inscription.html.twig",
                ["formulaire"=>$form->createView()]); 
        }
        else{
            return $this->redirectToRoute("infosUtilisateurs");
        }
    }
    
    /**
     * @Route("/Utilisateurs/connexionUtilisateurs", name="connexionUtilisateurs")
     */
    public function connexionUtilisateurs(Request $request)
    {
        $statut=$this->isConnected();
        if ($statut == false) {
            $utilisateur= array();
            if ($request->isMethod('POST')){
                $em=$this->getDoctrine()->getManager();
                $mail = $_POST['mail'];
                $pwd = $_POST['pwd'];
                $utilisateur=$em->getRepository("AppBundle:Utilisateur")->findOneBy(["mail"=>$mail,"password"=>$pwd]);
            }
            if (empty($utilisateur)) {
                $this->addFlash('danger', "Il y a eu erreur sur les champs renseignés ! Veuillez réessayer.");
                return $this->render("Utilisateurs/connexion.html.twig");
            }
            else{
                $_SESSION['utilisateur']=$utilisateur->getId();            
                return $this->redirectToRoute("infosUtilisateurs",["utilisateur"=>$utilisateur]);
            }
        }
        else{
            return $this->redirectToRoute("infosUtilisateurs");
        }
    }
    
    /**
     * @Route("/Utilisateurs/infosUtilisateurs", name="infosUtilisateurs")
     */
    public function infosUtilisateur(Request $request){
        $statut=$this->isConnected();
        $em=$this->getDoctrine()->getManager();
        if ($statut==false) {
            return $this->render("Utilisateurs/connexion.html.twig");
        }
        else{
            $id = $statut;
            $utilisateur=$em->getRepository("AppBundle:Utilisateur")->find($id);
            
            $sondages=$em->getRepository("AppBundle:Sondage")
                ->createQueryBuilder("e")
                ->innerJoin("e.questions_sondage","q")
                ->innerJoin("q.reponses","r")
                ->innerJoin("r.reponses_utilisateur","w")
                ->innerJoin("w.utilisateur","u")
                ->Where("u.id=:id")
                ->setParameter("id",$id)
                ->getQuery()
                ->getResult();
            $quizz=$em->getRepository("AppBundle:Quizz")
                ->createQueryBuilder("e")
                ->innerJoin("e.questions","q")
                ->innerJoin("q.reponses_quizzz","r")
                ->innerJoin("r.reponses_utilisateur","w")
                ->innerJoin("w.utilisateur","u")
                ->Where("u.id=:id")
                ->setParameter("id",$id)
                ->getQuery()
                ->getResult();
            $enquetes=$em->getRepository("AppBundle:Enquete")
                ->createQueryBuilder("f")
                ->innerJoin("f.sequences","e")
                ->innerJoin("e.questions","q")
                ->innerJoin("q.reponses","r")
                ->innerJoin("r.reponses_utilisateur","w")
                ->innerJoin("w.utilisateur","u")
                ->Where("u.id=:id")
                ->setParameter("id",$id)
                ->getQuery()
                ->getResult();
            
            $recompenses=$em->getRepository("AppBundle:Recompense")
                ->createQueryBuilder("e")
                ->innerJoin("e.quizz","qi")
                ->innerJoin("qi.questions","q")
                ->innerJoin("q.reponses_quizzz","r")
                ->innerJoin("r.reponses_utilisateur","w")
                ->innerJoin("w.utilisateur","u")
                ->Where("u.id=:id")
                ->setParameter("id",$id)
                ->getQuery()
                ->getResult();
                        
            return $this->render("Utilisateurs/account.html.twig",["utilisateur"=>$utilisateur,"sondages"=>$sondages,"quizz"=>$quizz,"enquetes"=>$enquetes,"recompense"=>$recompenses]);
        }
            
    }
    
    
    /**
     * @Route("/Utilisateurs/deconnexion", name="deconnexion")
     */
    public function deconnexion(Request $request)
    {
       session_destroy();
       return $this->redirectToRoute("connexionUtilisateurs");
    }
    
    
}