<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class QuizzController extends Controller
{
    /**
     * @Route("/Quizz/", name="listerQuizz")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        $em=$this->getDoctrine()->getManager();
        $quizz=$em->getRepository("AppBundle:Quizz")->findAll();
        return $this->render('Quizz/index.html.twig',["quizz"=>$quizz]);
    }
    
    
    /**
     * @Route("/Quizz/voir/{id}", name="voirQuizz"
     * , requirements={
     * "id":"\d+"
     * })
     */
    public function voirQuizz($id,Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $quizz=$em->getRepository("AppBundle:Quizz")->find($id);
        $questions=$em->getRepository("AppBundle:Question_Quizz")->findBy(["quizz"=>$id]);
        $reponses=$em->getRepository("AppBundle:Reponse_Quizz")->findBy(["question"=>$questions]);
        return $this->render('Quizz/voir.html.twig',["quizz"=>$quizz,"questions"=>$questions,"reponses"=>$reponses]);
    }
       
   
    
    /**
     * @Route("/Quizz/repondre/enregistrerReponseQuizz/{id}", name="enregistrerReponseQuizz"
     * , requirements={
     * "id":"\d+"
     * })
     */
    public function enregistrerReponseQuizz($id,Request $request)
    {   
        $id_quizz=$id;
        $statut=$this->isConnected();
        if ($statut==false) {
            $this->addFlash('danger', "Vous devez être connecté pour répondre aux enquêtes.");
            return $this->render("Utilisateurs/connexion.html.twig");
        }
        else{
            
            $em=$this->getDoctrine()->getManager();
            
            
            
            $utilisateur=$em->getRepository("AppBundle:Utilisateur")->findOneById($statut);
            $id_utilisateur=$utilisateur->getId();
            if ($request->isMethod('POST')) {
                $reponses=$_POST;
                
                $verif_utilisateur_reponses_quizz=$em->getRepository("AppBundle:utilisateur")->createQueryBuilder('u')
                    ->Join("u.reponses_quizz","urs")
                    ->Join("urs.reponse","rs")
                    ->Join("rs.question","qs")
                    ->Join("qs.quizz","q")
                    ->select("q.id")
                    ->where("u.id = :id_utilisateur")
                    ->andWhere("q.id = :id_quizz")
                    ->setParameter("id_utilisateur",$id_utilisateur)
                    ->setParameter("id_quizz",$id_quizz)
                    ->getQuery()
                    ->getResult();
                
                foreach ($reponses as $reponseX){
                    if (empty($verif_utilisateur_reponses_quizz)) {
                        $reponse_utilisateur = new \AppBundle\Entity\utilisateur_reponse_quizz();
                        $reponse = $em->getRepository("AppBundle:Reponse_Quizz")->findOneById($reponseX);
                        $reponse_utilisateur->setUtilisateur($utilisateur);
                        $reponse_utilisateur->setReponse($reponse);
                        $dateactuelle = new \DateTime(date("Y-m-d h:i:s"));
                        $reponse_utilisateur->setDateReponse($dateactuelle);
                            $em->persist($reponse_utilisateur);
                            $em->flush();
                            $a_message=true;
                        }
                    else{
                        $a_message=false;
                    }
                }
            }
            if ($a_message==true) {
                $this->addFlash('notice', "Votre réponse a bien été enregistrée !");
            }
            else{
                $this->addFlash('error', "Vous avez déjà répondu à ce quizz !");
            }
            $quizz=$em->getRepository("AppBundle:Quizz")->findAll();
            return $this->render('Quizz/index.html.twig',["quizz"=>$quizz]);
        }
    }
    
}

    
