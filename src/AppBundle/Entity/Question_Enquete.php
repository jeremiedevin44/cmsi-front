<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Question_Enquete
 *
 * @ORM\Table(name="question__enquete")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Question_EnqueteRepository")
 */
class Question_Enquete
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="intitule", type="string", length=255)
     */
    public $intitule;
    
    /**
     * @var int
     * 
     * @ORM\Column(name="ordre", type="integer")
     */
    public $ordre;
    
    /**
    * @ORM\ManyToOne(targetEntity="Sequence", inversedBy="questions")
    * @ORM\JoinColumn(name="sequence_id", referencedColumnName="id")
    */
    private $sequence; // 
    
    /**
     * @ORM\OneToMany(targetEntity="Reponse_Enquete", mappedBy="question")
     */
    private $reponses;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reponses = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set intitule
     *
     * @param string $intitule
     *
     * @return Question_Enquete
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;

        return $this;
    }

    /**
     * Get intitule
     *
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * Set ordre
     *
     * @param integer $ordre
     *
     * @return Question_Enquete
     */
    public function setOrdre($ordre)
    {
        $this->ordre = $ordre;

        return $this;
    }

    /**
     * Get ordre
     *
     * @return integer
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * Add reponse
     *
     * @param \AppBundle\Entity\Reponse_Enquete $reponse
     *
     * @return Question_Enquete
     */
    public function addReponse(\AppBundle\Entity\Reponse_Enquete $reponse)
    {
        $this->reponses[] = $reponse;

        return $this;
    }

    /**
     * Remove reponse
     *
     * @param \AppBundle\Entity\Reponse_Enquete $reponse
     */
    public function removeReponse(\AppBundle\Entity\Reponse_Enquete $reponse)
    {
        $this->reponses->removeElement($reponse);
    }

    /**
     * Get reponses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReponses()
    {
        return $this->reponses;
    }

    /**
     * Set questionsSequencesEnquete
     *
     * @param \AppBundle\Entity\Sequence $questionsSequencesEnquete
     *
     * @return Question_Enquete
     */
    public function setQuestionsSequencesEnquete(\AppBundle\Entity\Sequence $questionsSequencesEnquete = null)
    {
        $this->questions_sequences_enquete = $questionsSequencesEnquete;

        return $this;
    }

    /**
     * Get questionsSequencesEnquete
     *
     * @return \AppBundle\Entity\Sequence
     */
    public function getQuestionsSequencesEnquete()
    {
        return $this->questions_sequences_enquete;
    }

    /**
     * Set sequence
     *
     * @param \AppBundle\Entity\Sequence $sequence
     *
     * @return Question_Enquete
     */
    public function setSequence(\AppBundle\Entity\Sequence $sequence = null)
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * Get sequence
     *
     * @return \AppBundle\Entity\Sequence
     */
    public function getSequence()
    {
        return $this->sequence;
    }
}
